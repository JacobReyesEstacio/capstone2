const Image = require("../models/Image");
const Product = require ("../models/Product");

module.exports.upload = async (req, res) => {

		const { base64, productName } = req.body;

    try {
     		await Image.create({
     		image: base64,
     		productName: productName
     	}

     	);
     		res.send({Status:"ok"})

      }
      
    catch (error) {

    	res.send({Status:"error",data:error})

      };  
    }
  

module.exports.grab = async (req, res) => {

  const productName = req.body.productName;

  try {
    const image = await Image.findOne({ productName: productName });
    
    if (image) {
      // If an image with the productName exists, send it in the response
      res.send(image);
      console.log(image)
    } else {
      // If no image is found, send an appropriate message
      res.send({ Status: "not found", data: "Image not found for productName: " + productName });
    }
  } catch (error) {
    res.send({ Status: "error", data: error });
  }
};