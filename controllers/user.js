const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");



module.exports.registerUser = async (req, res) => {
    try {
      const email = req.body.email;
  
      // Check if email already exists
      const existingUser = await User.findOne({ email });
  
      if (existingUser) {
        return res.status(400).json({ message: 'The email already exists' });
      }
  
      // If email doesn't exist, proceed with registration
      const newUser = new User({
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10)
      });
      // Save the newUser
      newUser.save()
        .then(() => {
          console.log(newUser.email);
          return res.send(true);
        })
        .catch(error => {
          console.error(error);
          return res.status(500).json({ message: 'An error occurred while saving user data' });
        });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'An error occurred' });
    }
  };


module.exports.loginUser = (req, res) => {
    return User.findOne({ email: req.body.email })
      .then((result) => {
        if (result === null) {
          return false;
        } else {
          //compare a non-encrypted password and the encrypted password from the DB
          const isPasswordCorrect = bcrypt.compareSync(
            req.body.password,
            result.password
          );
          // If PW matches
          if (isPasswordCorrect) {
            return res.send({ access: auth.createAccessToken(result) });
          }
          // Else PW does not match
          else {
            return res.send(false);
          }
        }
      })
      .catch((err) => res.send(err));
  };
//Retrieve logged in User's Information
module.exports.retrieveInfo = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).send("User not found");
    }

    res.send(user);
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occurred while processing your request.");
  }
};
//Set a user as Admin (Admin only)
module.exports.setAdmin = async (req, res) => {
  try {
    const user = await User.findById(req.body.id);

    if (!user) {
      return res.send("User not found");
    }else {
      user.isAdmin = true;
      await user.save();
      res.send(`admin promotion success!`);
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occurred while processing your request.");
  }
}



module.exports.getAllUsers = async (req, res) => {
  try {
   
    const user = await User.findById(req.user.id);

    if (!user || !user.isAdmin) {
      return res.status(403).json({ message: 'Access denied. Only admins can access this resource.' });
    }
    const users = await User.find();

    res.status(200).json(users);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while fetching users.' });
  }
};
  