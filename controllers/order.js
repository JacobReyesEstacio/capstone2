const User = require("../models/User");
const Order = require("../models/Order");
const Product = require ("../models/Product");

//Checkout Order
module.exports.checkout = async (req, res) => {
    try {
      // Check if logged in user is Admin
      if (req.user.isAdmin) {
        return res.send("Please login to a user account. Admins do not need to use this function");
      } else {
        const orderProducts = req.body.orderProducts;
  
        let totalAmount = 0;
  
        for (const orderProduct of orderProducts) {
          const product = await Product.findById(orderProduct.productId);
          if (product) {
            totalAmount += product.productPrice * orderProduct.quantity;
          }
        }
  
        const newOrder = new Order({
          orderUserId: req.user.id,
          orderProducts: orderProducts,
          totalAmount: totalAmount
        });
  
        // Save the newOrder to the database and handle success/failure
        await newOrder.save();
  
        return res.send("Order placed successfully!");
      }
    } catch (error) {
      console.error(error);
      return res.status(500).send("An error occurred while processing your request.");
    }
  };

//Get Order Details
module.exports.getOrderDetails = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    
    if (!user) {
      return res.send("User not found");
    }

    if (user.isAdmin) {
      return res.send("Action forbidden");
    }

    const orders = await Order.find({ orderUserId: user.id }); 

    res.send(orders);
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occurred while processing your request.");
  }
}

//Get all Orders (Admin Only)

module.exports.getAllOrders = async (req, res) => {
  try {
    const orders = await Order.find({});
    res.send (orders);
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occured while processing your request.");
  }
} 

//Get Order Details by OrderUserID

//Get Order Details only admin
module.exports.getOrderDetailsByUserId = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    
    if (!user) {
      return res.send("User not found");
    }

    if (user.isAdmin) {
      return res.send("Action forbidden");
    }

    const orders = await Order.find({ orderUserId: user.id }); 

    res.send(orders);
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occurred while processing your request.");
  }
}





