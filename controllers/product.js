const Product = require ("../models/Product");
const User = require ("../models/User");


//Add a product (Admin Only)
module.exports.addProduct = async (req, res) => {
    if (!req.user.isAdmin) {
      return res.status(403).send("Access Denied");
    }
  
    const newProduct = new Product({
      productName: req.body.productName,
      productDescription: req.body.productDescription,
      productPrice: req.body.productPrice,
    });
  
    try {
      const savedProduct = await newProduct.save();
      console.log(savedProduct);
      res.send(true);
    } catch (error) {
      console.error(error);
      res.send(false);
    }
  };

//View all Products active & inactive (Admin only)
module.exports.getAllProducts = async (req, res) => {
    try {
      if (!req.user) {
        return res.send("Please log in to access this information");
      }
  
      if (!req.user.isAdmin) {
        return res.status(403).send("Access Denied");
      }
  
      const products = await Product.find({});
      res.send(products);
    } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
    }
  };

//View all active products   
  module.exports.getAllActiveProducts = async (req, res) => {
    try {
      const products = await Product.find({ isActive: true });
      res.send(products);
    } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
    }
  };

//View a product by ID
module.exports.getProduct = async (req, res) => {
    try {
      const product = await Product.findById(req.params.productId);
      res.send(product);
    } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
    }
  };

//Update Product information (Admin Only)
module.exports.updateProduct = async (req, res) => {
    try {
      if (!req.user || !req.user.isAdmin) {
        return res.status(403).send("Access Denied");
      }
  
      const updatedProduct = {
        productName: req.body.productName,
        productDescription: req.body.productDescription,
        productPrice: req.body.productPrice
      };
  
      const product = await Product.findByIdAndUpdate(req.params.productId, updatedProduct);
  
      if (!product) {
        return res.send(false);
      } else {
        res.send(true);
      }
    } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
    }
  };

//Archive a product (Admin only)
module.exports.archiveProduct = async (req, res) => {
  try {
    const product = await Product.findByIdAndUpdate(req.params.productId, { isActive: false });

    if (product) {
      return res.send(true);
    } else {
      return res.send(false);
    }
  } catch (error) {
    return res.send(false);
  }
};
//Activate a product (Admin only)
module.exports.activateProduct = async (req, res) => {
  try {
    const productId = req.params.productId;

    const product = await Product.findByIdAndUpdate(productId, { isActive: true });

    if (product) {
      return res.send(true);
    } else {
      return res.send(false);
    }
  } catch (error) {
    return res.send(false);
  }
};


module.exports.createProduct = async (req, res) => {
  try {
    // Extract product data from request
    const { productName, productDescription, productPrice } = req.body;
    const image = req.file;

    // Create a new product instance
    const product = new Product({
      productName,
      productDescription,
      productPrice,
      image: {
        filename: image.filename,
        path: image.path,
      }
    });

    // Save the product to the database
    await product.save();

    res.json({ message: 'Product created successfully!' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error creating product' });
  }
};
