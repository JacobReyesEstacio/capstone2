const User = require("../models/User");
const Cart = require("../models/Cart");
const Product = require ("../models/Product");
const Order = require ("../models/Order");

//Add to cart
module.exports.addToCart = async (req, res) => {
    try {
      const user = await User.findById(req.user.id);
      const product = await Product.findById(req.body.productId);
  
      if (!user || !product) {
        return res.send("User or product not found");
      }
      
      const cartItem = new Cart({
        cartUserId: user._id, 
        productId: product._id,
        quantity: req.body.quantity
      });
      
      await cartItem.save();
      res.send("Product added to cart");
    } catch (error) {
      console.error(error);
      return res.status(500).send("An error occurred while processing your request.");
    }
  }

//Update Cart change quantity
module.exports.updateCart = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    const product = await Product.findById(req.body.productId);

    if (!user || !product) {
      return res.send("User or product not found");
    }

    // Find the cart item for the user and the specific product
    const cartItem = await Cart.findOne({
      cartUserId: user._id,
      productId: product._id
    });

    if (!cartItem) {
      return res.send("Product not found in cart");
    }

    // Update the quantity of the specific product in the cart
    cartItem.quantity = req.body.quantity; // Update the quantity based on the request
    await cartItem.save();
    return res.send("Cart updated");
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occurred while processing your request.");
  }
};





module.exports.getAllCart = async (req, res) => {
  try {
    const cartItems = await Cart.find({ cartUserId: req.user.id });
    res.send (cartItems);
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occured while processing your request.");
  }
} 


// Delete one item from the cart by productId
module.exports.deleteCartItem = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    const product = await Product.findById(req.body.productId);

    if (!user || !product) {
      return res.send("User or product not found");
    }

    // Find and delete the cart item based on the user and product IDs
    await Cart.findOneAndDelete({
      cartUserId: user._id,
      productId: product._id
    });

    return res.send("Product removed from cart");
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occurred while processing your request.");
  }
};

module.exports.clearCart = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.send("User or product not found");
    }

    await Cart.deleteMany({ cartUserId: user._id});

    return res.send("Product removed from cart");
  } catch (error) {
    console.error(error);
    return res.status(500).send("An error occurred while processing your request.");
  }
};

