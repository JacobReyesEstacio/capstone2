//DEPENDENCIES AND MODULES
const express = require ('express')
const mongoose = require ('mongoose')
const cors = require ("cors")
const port = 4000;
const app = express();
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");
const cartRoutes = require("./routes/cart");
const imageRoutes = require("./routes/image");


//MIDDLEWARE
app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({extended : true}));
app.use(cors());
app.use('/uploads', express.static('uploads'))

mongoose.connect(
    "mongodb+srv://Jacob:flr88ax45bvg@cluster0.q6y4s9e.mongodb.net/capstone2?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

//BACKEND ROUTES
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/carts", cartRoutes);
app.use("/images", imageRoutes);



//SERVER GATEWAY RESPONSE
if (require.main === module) {
    app.listen(process.env.PORT || port, () => {
        console.log(`API is now online on Port ${process.env.PORT || port}`);
    });
}

module.exports = { app, mongoose };
