 ## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: user@gmail.com
     - pwd: user123
- Admin User:
    - email: admin@gmail.com
    - pwd: admin123
    

***ROUTES:***
- User registration (POST)
	- http://localhost:4000/users/register
    - request body: 
        - email (string)
        - password (string)
- User authentication / login (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)
- Create Product (Admin only) (POST)
	- http://localhost:4000/products/addProduct
    - request body: 
        - productName (string)
        - productDescription (string)
        - productPrice (number)
- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/products/getAllProducts
    - request body: none
- Retrieve all active products (GET)
    - http://localhost:4000/products/getAllActiveProducts
    - request body: none
- Retrieve single product by ID (GET)
    - http://localhost:4000/products/:productId
    - request body: none
- Update Product (Admin only) (PUT)
    - http://localhost:4000/products/updateProduct/:productId
    - request body: 
        - productName (string)
        - productDescription (string)
        - productPrice (number)
- Archive Product (Admin only) (PUT)
    - http://localhost:4000/products/archiveProduct/:productId
    - request body: none
- Activate Product (Admin only) (PUT)
    - http://localhost:4000/products/activateProduct/:productId
    - request body: none
- Order Checkout (USER only) (POST)
    - http://localhost:4000/orders/checkout
    - request body: 
        - orderProducts (Array of objects)
        - productId (string)
        - quantity (number)
- Retrieve logged in user's information (GET)
    - http://localhost:4000/users/info
    - request body: none
- Set User as Admin (Admin only) (PUT)
    - http://localhost:4000/users/setAdmin
    - request body: 
        - id (string)
- View Order of logged in User
    - http://localhost:4000/orders/getOrders
- View All Orders (Admin Only)
    -http://localhost:4000/orders/orders
- Add to cart
    -http://localhost:4000/carts/addToCart
    -request body:
        - productId (String)
        - quantity (Number)
- Update Cart
    -http://localhost:4000/carts/updateCart
    - request body:
        - productId (String)
        - quantity (Number)
- Get Cart
    -http://localhost:4000/carts/allCart

        
        
