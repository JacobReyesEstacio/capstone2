//Dependencies
const express = require('express');
const cartController = require('../controllers/cart');
const auth = require ("../auth");
// Routing Component
const router = express.Router();
const { verify, verifyAdmin } = auth;

// Route to add an item to the cart
router.post("/addToCart", verify, cartController.addToCart);

// Route to update item quantity on the cart
router.put("/updateCart", verify, cartController.updateCart);

router.get("/allCart", verify, cartController.getAllCart);

router.delete("/removeCart", verify, cartController.deleteCartItem);

router.delete("/clearCart", verify, cartController.clearCart);

module.exports = router;