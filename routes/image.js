const express = require('express');
const imageController = require('../controllers/image');
const auth = require ("../auth");
// Routing Component
const router = express.Router();
const { verify, verifyAdmin } = auth;


router.post("/upload-image", verify, verifyAdmin, imageController.upload);

router.post("/get-image", verify, imageController.grab);





module.exports = router;