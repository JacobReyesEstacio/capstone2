// Dependencies
const express = require("express");
const orderController = require("../controllers/order");
const auth = require ("../auth");
// Routing Component
const router = express.Router();
const { verify, verifyAdmin } = auth;

//Checkout
router.post("/checkout", verify, orderController.checkout);

//Get Order Details
router.get("/getOrders", verify, orderController.getOrderDetails);

//Get All Order Details
router.get("/orders", verify, verifyAdmin, orderController.getAllOrders);

router.post("/orders/sale/:userId", verify, verifyAdmin, orderController.getOrderDetailsByUserId);













// [Export Route System]
module.exports = router;