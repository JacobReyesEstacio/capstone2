// Dependencies
const express = require("express");
const productController = require("../controllers/product");
const auth = require ("../auth");
// Routing Component
const router = express.Router();
const { verify, verifyAdmin } = auth;
const multer = require('multer');

const storage = multer.diskStorage({
   destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Specify the directory where product images will be uploaded
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const upload = multer({ storage });

//Add a Product
router.post("/addProduct", verify, verifyAdmin, productController.addProduct);
//View all Products
router.get("/getAllProducts", verify, verifyAdmin, productController.getAllProducts);
//View all active Products
router.get("/getAllActiveProducts", productController.getAllActiveProducts);
//View Product by ID
router.get("/:productId", productController.getProduct);
//Update a Product if user is admin
router.put("/updateProduct/:productId", verify, verifyAdmin, productController.updateProduct);
//Archive Product if user is admin
router.put("/archiveProduct/:productId", verify, verifyAdmin, productController.archiveProduct);
//Activate Product if user is admin
router.put("/activateProduct/:productId", verify, verifyAdmin, productController.activateProduct)
//upload with image
router.post('/create-product', upload.single('image'), verify, verifyAdmin, productController.createProduct);



// [Export Route System]
module.exports = router;