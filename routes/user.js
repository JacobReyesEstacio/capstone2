// Dependencies
const express = require("express");
const userController = require("../controllers/user");
const auth = require ("../auth");
// Routing Component
const router = express.Router();
const { verify, verifyAdmin } = auth;


// User Registration
router.post("/register", userController.registerUser);

// User Login
router.post("/login", userController.loginUser);

// Retrieve User Details

router.get("/info", verify, userController.retrieveInfo);

// Set user as Admin

router.put("/setAdmin", verify, verifyAdmin, userController.setAdmin);


// https://cpston2-ecommerceapi-estacio.onrender.com/users/allUsers
router.get("/allUsers", verify, verifyAdmin, userController.getAllUsers);









// [Export Route System]
module.exports = router;