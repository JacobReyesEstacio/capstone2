const jwt = require("jsonwebtoken");
const secret = "EstacioEcommerceAPI";

//JSON Web Tokens
//JWT is a way of securely passing information from the server to the frontned or to other parts of the server

//Token creation


module.exports.createAccessToken = (user)=>{
	//the data from the user will be received through forms/req.body
	//when the user logs in, a TOKEN will be created with the user's information
	const data = {
		id: user._id,
		email:user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
}

//Token verification

module.exports.verify = (req,res,next)=>{

	//req.headers.authorization contains sensitive data and especially our token
	console.log("This is from req.headers.authorization")
	console.log(req.headers.authorization)

	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth:"Failed. No Token"});
	}else{
		
		console.log("With bearer prefix")
		console.log(token);
		token = token.slice(7,token.length)
		console.log("No bearer prefix")
		console.log(token);

		jwt.verify(token, secret, function(err,decodedToken){
			

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			}else{
				console.log("data that will be assigned to the req.user")
				console.log(decodedToken);

				req.user = decodedToken
				next()
				//middleware function
				//next() will let us proceed to the next middleware OR controller
			}
		})
	}
}
//Admin verification
module.exports.verifyAdmin = (req,res,next)=>{

	//verifyAdmin comes AFTER the verify middleware

	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			auth:"Failed",
			message:"Action Forbidden"
		})
	}
}
