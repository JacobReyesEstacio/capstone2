const mongoose = require('mongoose');

// CART SCHEMA
const cartSchema = new mongoose.Schema({
    // ID of the user who added the products to the cart (Must be associated with UserID from user model)
    cartUserId: {
        type: String,
        required: [true, "cartUserId is required"]
    },
    // Products added to the cart
   
    productId: {
        type: String,
        required: [true, "Product ID is required"]
            },
     quantity: {
        type: Number,
        required: [true, "Product Quantity is required"]
            },
    // Date when the items were added to the cart
    addedOn: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("Cart", cartSchema);