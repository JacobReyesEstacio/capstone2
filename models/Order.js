const mongoose = require('mongoose');

//ORDER SCHEMA
const orderSchema = new mongoose.Schema({
    //ID of the user who ordered the products (Must be associated with UserID from user model)
    orderUserId : {
        type : String,
        required : [true, "orderId is required"]
    },
    //Products ordered 
    orderProducts : [
        {
            productId : {
                type : String,
                required : [true, "Product ID is required"]
            },

            quantity : {
                type : Number,
                required : [true, "Product Quantity is required"]
            }
        }
    ],
    //Amount of orders made by the user from orderUserId
    totalAmount : {
        type : Number,
        required : [true, "Total amount is required"]
    },
    //Date of the order
    purchasedOn : {
        type : Date,
        default : new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema);