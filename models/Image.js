const mongoose = require('mongoose');

const imageSchema = new mongoose.Schema({
  image: String, // Assuming the image data is stored as a string
  productName: String,
});

module.exports = mongoose.model('Image', imageSchema);
