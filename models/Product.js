const mongoose = require('mongoose')

//PRODUCT SCHEMA
const productSchema = new mongoose.Schema({
    //Name of the Product
    productName : {
        type : String,
        required : [true, "Product name is required"]
    },
    //Description of the Product
    productDescription : {
        type : String,
        required : [true, "Product description is required"]
    },
    //Price of the Product
    productPrice : {
        type : Number,
        required : [true, "Product price is required"]
    },
    //Determines if Product is still available
    isActive : {
        type : Boolean,
        default : true
    },
    //Date when product was inducted to the database
    createdOn : {
        type : Date,
        default : new Date()
    }

})

module.exports = mongoose.model("Product", productSchema);
