const mongoose = require('mongoose');

// SCHEMA
const userSchema = new mongoose.Schema({
    //User's Email
    email : {
        type : String,
        required: [true, "Email is required"]
    },
    //User's Password
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    //Determine if User is an Admin
    isAdmin : {
        type : Boolean,
        default : false
    }

});

module.exports = mongoose.model("User", userSchema);